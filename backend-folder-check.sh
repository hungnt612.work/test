#!/bin/bash
echo 'Start checking directory App'
datetime=$(date +"%Y-%m-%d %T")
while inotifywait -r /home/tuanhung0601/digitalcard/app -e create,delete,modify;
do { echo "App directory changed at $datetime";
    echo "Reloading...";
    cd client &&
    ~/.nvm/nvm.sh &&
    echo "Install Dependency" &&
    yarn install &&
    yarn start &&
    pm2 reload app &&
    echo "Finished"
    echo "con cho tri in app $datetime" > test.txt
    }; done 