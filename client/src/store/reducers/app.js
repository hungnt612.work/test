import { handleActions } from "redux-actions";

import actions from "../actions/app";

export const name = "app";
const initialState = {
  title: ""
};

const reducerMap = {
  [actions.setTitle]: (state, { payload }) => {
    return {
      ...state,
      title: payload
    };
  }
};
export default handleActions(reducerMap, initialState);
