import actions from "./actions";

export const setTitle = name => async dispatch => {
  dispatch(actions.setTitle(name));
};
