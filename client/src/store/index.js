import rootReducers from "./reducers";
import { combineReducers, createStore } from "redux";

const rootReducer = combineReducers(rootReducers);

const store = createStore(rootReducer, {});

export default store;
