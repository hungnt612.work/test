import React from 'react';
import classes from './guest.scss';

const guestLayout = Component => {
    return (
        <div className={classes.root}>
            <Component />
        </div>
    );
};

export default guestLayout;
