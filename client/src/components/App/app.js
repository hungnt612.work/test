import React from "react";
import Router from "@components/Router";

const App = props => {
  return (
    <div>
      <Router />
    </div>
  );
};

export default App;
