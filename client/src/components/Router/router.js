import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Layout
import { guestLayout } from '@components/Layout';

// Guest Component
import SignInPage from '@components/SignInPage';

/*
* TODO: Write routers in routers.json
 */

const AppRouter = props => {
    return (
        <Router>
            <Switch>
                <Route path="/user/signIn">{guestLayout(SignInPage)}</Route>
                <Route path="/">{guestLayout(SignInPage)}</Route>
            </Switch>
        </Router>
    );
};
export default AppRouter;
