import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from '@components/App';

import store from './store';
import ThemeContextProvider from './context';
import './ant.less';
import classes from './index.css';

ReactDOM.render(
    <React.Fragment>
        <Provider store={store}>
            <ThemeContextProvider>
                <App />
            </ThemeContextProvider>
        </Provider>
    </React.Fragment>,
    document.getElementById('root')
);
