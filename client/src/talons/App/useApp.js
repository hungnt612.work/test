import React, { useState } from "react";

// TODO: Will write logic functions here */
export const useApp = props => {
  const [items, setItems] = useState();
  return { items, setItems };
};
